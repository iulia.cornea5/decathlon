package collections;

import com.company.product.InedibleProduct;
import com.company.product.Product;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class CollectionsMain {

    public static void main(String[] args) {

        String[] arrayOfStringsOfFixedSize = new String[100];
        arrayOfStringsOfFixedSize[0] = "2123";
        System.out.println(arrayOfStringsOfFixedSize.length);
        arrayOfStringsOfFixedSize[1] = "2123";
        System.out.println(arrayOfStringsOfFixedSize.length);
        arrayOfStringsOfFixedSize[2] = "2123";

        ArrayList<String> arrayOfStringsOfVariableSize = new ArrayList<>();

        ArrayList<Integer> ints = new ArrayList<>();
        ints.add(123);
        ints.add(13);
        ints.add(1311);
        ints.add(11);

        ArrayList<String> strings = new ArrayList<>();
        strings.add("primul");
        strings.add("al doilea");
        strings.add("al treilea");


        ArrayList<Product> products = new ArrayList<>();
        products.add(new InedibleProduct("bikes", 10));
        System.out.println(products.size());
        products.add(new InedibleProduct("weights", 10));
        products.add(new InedibleProduct("foam rollers", 10));
        System.out.println(products.size());
        products.add(new InedibleProduct("mountain bikes", 10));


        for(int i= 0; i<products.size(); i++) {
            System.out.println(products.get(i).getName());
        }

        products.remove(1);
        products.add(new InedibleProduct("weights", 10)); // id1237126378


        products.remove(new InedibleProduct("weights", 10)); // id00000232312

        System.out.println("weights removed?");
        for(int i= 0; i<products.size(); i++) {
            System.out.println(products.get(i).getName());
        }

        Set<InedibleProduct> productsSet = new LinkedHashSet<>();
        productsSet.add(new InedibleProduct("bikes", 10));
        productsSet.add(new InedibleProduct("weights", 10));
        productsSet.add(new InedibleProduct("weights", 10));
        productsSet.add(new InedibleProduct("weights", 10));
        productsSet.add(new InedibleProduct("weights", 10));
        productsSet.add(new InedibleProduct("foam rollers", 10));
        productsSet.add(new InedibleProduct("mountain bikes", 10));

//        InedibleProduct p = new InedibleProduct("weights", 10);
//        p.equals(p);

//        for(int i= 0; i<productsSet.size(); i++) {
//            System.out.println(productsSet.get(i).getName());
//        }

        productsSet.forEach(p -> {
            System.out.println(p.getName());
        });

    }
}
