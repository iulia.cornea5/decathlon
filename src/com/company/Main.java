package com.company;


/*
    Protein Bar / Water Bottle / Weights / Bicycle / Pants / Smart Watch

    Products -> Edible / Inedible
    NeedsService


 */

import com.company.product.InedibleProduct;
import com.company.product.InedibleWithMaintenanceProduct;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {

        InedibleProduct bicycle =
                new InedibleProduct(
                        "MountainBike",
                        10,
                        2,
                        "Shimano Service");

        InedibleProduct bicycle2 =
                new InedibleProduct(
                        "RoadBike",
                        10,
                        2);

        InedibleProduct bicycle3 =
                new InedibleProduct(
                        "RoadBike",
                        10);

        System.out.println(bicycle.getService());

        InedibleWithMaintenanceProduct computer = new InedibleWithMaintenanceProduct("Computer", 20,2,"Service", LocalDate.now().minusYears(2),1);
        computer.isAvailableForMaintenance();
    }
}
