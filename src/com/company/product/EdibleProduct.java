package com.company.product;

import java.time.LocalDate;
import java.util.Date;

public class EdibleProduct extends Product{

    private String barCode;

    private LocalDate expirationDate;

    public EdibleProduct(String name, int quantity, LocalDate expirationDate) {
        super(name, quantity);
        this.expirationDate = expirationDate;
    }
}
