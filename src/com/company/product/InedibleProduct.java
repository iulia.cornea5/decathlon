package com.company.product;

public class InedibleProduct extends Product {

    private int warranty;
    private String service;

    public InedibleProduct(String name, int quantity, int warranty, String serviceP) {

        super(name, quantity);
        this.warranty = warranty;
        this.service = serviceP;
    }

    public InedibleProduct(String name, int quantity, int warranty) {
        super(name, quantity);
        this.warranty = warranty;
        this.service = "Decathlon Service";
    }

    public InedibleProduct(String name, int quantity) {
        super(name, quantity);
        this.warranty = 2;
        this.service = "Decathlon Service";
    }

    public int getWarranty() {
        return warranty;
    }

    public void setWarranty(int warranty) {
        this.warranty = warranty;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof InedibleProduct) {
            InedibleProduct p = (InedibleProduct) o;
            if (this.getName().equals(p.getName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return super.getName().hashCode();
    }
}
