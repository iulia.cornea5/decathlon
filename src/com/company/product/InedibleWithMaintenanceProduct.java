package com.company.product;

import java.time.LocalDate;

public class InedibleWithMaintenanceProduct extends InedibleProduct implements NeedsMaintenance {

    private LocalDate lastMaintenanceDate;
    private int maintenancePeriodInYears;

    public InedibleWithMaintenanceProduct(String name, int quantity, int warranty, String serviceP, LocalDate lastMaintenanceDate, int maintenancePeriodInYears) {
        super(name, quantity, warranty, serviceP);
        this.lastMaintenanceDate = lastMaintenanceDate;
        this.maintenancePeriodInYears = maintenancePeriodInYears;
    }


    @Override
    public void isAvailableForMaintenance() {
        if (LocalDate.now().isAfter(lastMaintenanceDate.plusYears(maintenancePeriodInYears))){
            System.out.println("The product is available for maintenance");
        }
        else{
            System.out.println("The product is not available for maintenance");
        }
    }
    // last maintenance date
    // maintenance period
}
