package com.company;

import com.company.product.EdibleProduct;
import com.company.product.InedibleProduct;
import com.company.product.Product;

import java.io.*;
import java.time.LocalDate;

public class ProductReader {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader("products.txt"));
        BufferedWriter writer = new BufferedWriter(new FileWriter("products_output.txt"));


        Product[] products = new Product[100];
        String line;
        int count = 0;
        while ((line = reader.readLine()) != null) {
            String[] infos = line.split("~");
            if (infos[0].equals("E")) {
                String name = infos[1];
                int quantity = Integer.parseInt(infos[2]);
                String initialDate = infos[3];
                // 10/12/2022 -> 2022-12-10
                String[] ddMMyyyy = initialDate.split("/");
                String finalDate = ddMMyyyy[2] + "-" + ddMMyyyy[1] + "-" + ddMMyyyy[0];
                LocalDate date = LocalDate.parse(finalDate);
                EdibleProduct e = new EdibleProduct(name, quantity, date);
                products[count] = e;
                count++;
            } else if (infos[0].equals("I")){
                String name = infos[1];
                int quantity = Integer.parseInt(infos[2]);
                int warranty = Integer.parseInt(infos[3]);
                String serviceP = infos[4];
                InedibleProduct e = new InedibleProduct(name,quantity,warranty,serviceP);
                products[count] = e;
                count++;
            }

        }

        for (int i = 0; i < products.length; i++) {
            if (products[i] != null){
                System.out.println(products[i].getName());
            writer.write(products[i].getName());
            writer.write('\n');}
        }


        reader.close();
        writer.close();
    }
}
